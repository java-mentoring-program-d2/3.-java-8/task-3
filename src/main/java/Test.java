import java.time.DayOfWeek;

public class Test {
    public static void main(String[] args) {
        //First example
        DayOfWeek dayOfWeek = DayOfWeek.SATURDAY;
        Integer amountOfHours = 8;
        Integer earningsPerHour = 100;
        ThreeFunction<DayOfWeek, Integer, Integer, Double> earningsCounter = (d, a, e) -> {
            double index = 1.0;
            if (d.equals(DayOfWeek.SATURDAY) || d.equals(DayOfWeek.SUNDAY)) {
                index = 1.5;
            }
            return index * a * e;
        };

        Double earningsPerDay = earningsCounter.threeFunction(dayOfWeek, amountOfHours, earningsPerHour);
        System.out.println(earningsPerDay);


        //Second example
        ThreeFunction<Double, Double, Double, Boolean> isEnoughToBuy = (e, t, c) -> e * (1 - t) >= c;

        Double taxProcent = 0.13;
        Double costOfIphone = 1400.0;

        Boolean isEnoughToBuyIphone = isEnoughToBuy.threeFunction(earningsPerDay, taxProcent, costOfIphone);
        System.out.println(isEnoughToBuyIphone);
    }
}
