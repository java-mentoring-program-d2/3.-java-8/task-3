import java.time.DayOfWeek;

@FunctionalInterface
public interface ThreeFunction<W, I, L, D> {
    D threeFunction(W w, I i, L l);
}
